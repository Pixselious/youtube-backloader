FROM ubuntu:jammy

RUN apt update && \
    apt install -y python3 python3-pip sqlite3 ffmpeg nginx openssl



WORKDIR /app

COPY . /app

# Install Python dependencies
RUN python3 -m pip install -r ./requirements.txt

# Configure Nginx
COPY nginx.conf /etc/nginx/sites-available/default


WORKDIR /app/backloader

# Ensure database
RUN python3 manage.py migrate

# Ensure templates
WORKDIR /app/backloader/backend
RUN python3 add_templates.py
WORKDIR /app/backloader

# Build the static files
RUN python3 manage.py collectstatic --noinput


# Expose ports
EXPOSE 80

CMD sh -c "nginx && python3 manage.py run_huey & gunicorn -w 4 -k uvicorn.workers.UvicornWorker --forwarded-allow-ips '*' backloader.asgi:application -b 0.0.0.0:8000"
