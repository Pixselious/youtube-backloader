import json
import sqlite3

# Load data from templates.json
with open('./templates.json', 'r') as file:
    data = json.load(file)

# Connect to the SQLite database
conn = sqlite3.connect('./../db.sqlite3')
cursor = conn.cursor()

# Check if the table is empty
cursor.execute('SELECT COUNT(*) FROM backend_template')
result = cursor.fetchone()

if result[0] > 0:
    print("The table is not empty. Script stopped without adding anything.")
    conn.close()
    exit()

# Insert data into the database
for item in data:
    cursor.execute('''
        INSERT INTO backend_template (name, type, content)
        VALUES (?, ?, ?)
    ''', (item['name'], item['type'], (str(item['content']) if item['type'] == 'codec' else json.dumps(item['content']))))

# Commit and close the connection
conn.commit()
conn.close()
