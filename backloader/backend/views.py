from rest_framework.views import APIView
from rest_framework import status
from rest_framework.response import Response
from django.http import HttpResponse, JsonResponse
from django.utils import timezone
from django.shortcuts import get_object_or_404
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers
from asgiref.sync import async_to_sync
from yt_dlp import YoutubeDL
from huey import crontab
from huey.contrib.djhuey import db_periodic_task, periodic_task, task, lock_task, enqueue, signal, scheduled
import datetime
import json
import os
from PIL import Image
import threading
import uuid
import httpx
import requests
import subprocess
import croniter


from .models import Flow as FlowModel
from .models import Outlet as OutletModel
from .models import Template as TemplateModel




@db_periodic_task(crontab(minute='*/1'), priority = 1)
def master():
    # this master task runs every 5 minutes, gets schedules of all flows, checks if the flow should be ran and if so schedules it with the consumer to be ran shortly.

    current_time = datetime.datetime.now().replace(second=0, microsecond=0)

    flows = FlowModel.objects.all()
    for flow in flows:
        schedule = json.loads(flow.schedule)
        cron_string = f"{schedule['minute']} {schedule['hour']} {schedule['day']} {schedule['day_of_week']} {schedule['month']}"
        #check if current_time fits the cron statement

        # print("AAAAAAAAAA", cron_string)
        # print(current_time)

        # Check if the current time fits the cron statement
        if croniter.croniter.match(cron_string, current_time):
            print("Scheduling flow " + str(flow.flow_id))

            yt_dl_flow(flow_id=flow.flow_id, lock=str(flow.outlet))



def initialize():

    print("Initializing...")

    try:

        i = 0

        for timer in TimerModel.objects.all():

            try:

                timer_id = timer.timer_id
                flow_id = timer.flow_set.first().flow_id

                if not find_timer(timer_id):
                    print("created timer" + str(i))
                    i += 1
                    create_timer(timer.interval, flow_id, timer_id)

            except Exception as e:
                print("Timer startup failed: " + str(e))

    except Exception as e:
            print("Timer startup failed: " + str(e))


@task(priority = 3)
def yt_dl_flow(flow_id, lock):

    with lock_task(lock):

        flow_object = FlowModel.objects.get(flow_id=flow_id)


        format = ''

        print(str(flow_object.quality))
        print(f"VideoConvertor: {flow_object.codec}")

        match flow_object.quality:
            case 'a':
                format = 'bestaudio'
            case 'max':
                format = 'bestaudio+bestvideo/best'
            case _:
                format = 'bestaudio+bestvideo[height={}]/bestaudio+bestvideo/best'.format(str(flow_object.quality))



        try:
            ydl_args = [
                        '-P', flow_object.outlet.path,
                        '-P', 'temp:tmp',
                        '--ignore-errors',                      # Ignore errors caused by unavailable videos
                        '-o', flow_object.outlet.video,
                        '--quiet',                              # Suppress standard output messages
                        '--format', format,                     # Format code or id
                        '--write-thumbnail',                    # Write thumbnail image to disk
                        '-o', 'thumbnail:' + flow_object.outlet.thumbnail,
                        '--write-info-json',                     # Write video metadata to a .json file
                        '-o', 'infojson:' + flow_object.outlet.info,
                        '--download-archive', f"{flow_object.outlet.path}/downloaded.txt",  # File name where the download history is recorded
                        '--embed-subs',                         # Embed subtitles in the output file
                        '--audio-format', 'm4a',                   # Output audio in m4a format
                        '--recode-video',  'mp4',                   # Recode video to mp4
                        '--use-postprocessor', 'FFmpegCopyStream', # Force transcode
                        '--ppa', f"CopyStream: {flow_object.codec}", # Recode video to provided codec
                        '--embed-metadata',                    # Embed the subtitles, chapters, and other metadata when possible
                        '--embed-thumbnail',                    # Embed the thumbnail
                        '--write-thumbnail',                    # Write thumbnail image to disk
                        #'--convert-thumbnails', 'png',             # Convert thumbnails to png format
                        '--write-info-json',                      # Write video metadata to a .json file
                        '--sponsorblock-mark', 'all',               # Sponsorblock marker for ads to be cut
            ]

            command = ['yt-dlp'] + ydl_args + [flow_object.url]

            try:
                subprocess.run(command, check=True)
            except subprocess.CalledProcessError as error:
                print(f'Download failed with error: error')


        except Exception as e:
            print(e)



@task(priority = 2)
def yt_dl_drop(settings_json, outlet):

    with lock_task(outlet):

        try:
            settings = json.loads(settings_json)

            url = settings['url']
            quality = settings['quality']
            codec = settings['codec']

        except KeyError as e:
            print(e)
            return

        outlet_object = OutletModel.objects.get(id=outlet)


        format = ''

        print(str(quality))

        match quality:
            case 'a':
                format = 'bestaudio'
            case 'max':
                format = 'bestaudio+bestvideo/best'
            case _:
                format = 'bestaudio+bestvideo[height={}]/bestaudio+bestvideo/best'.format(str(quality))


        try:
            ydl_args = [
                        '-P', outlet_object.path,
                        '-P', 'temp:tmp',
                        '--ignore-errors',                      # Ignore errors caused by unavailable videos
                        '-o', outlet_object.video,
                        #'--quiet',                              # Suppress standard output messages
                        '--format', format,                     # Format code or id
                        '--write-thumbnail',                    # Write thumbnail image to disk
                        '-o', 'thumbnail:' + outlet_object.thumbnail,
                        '--write-info-json',                     # Write video metadata to a .json file
                        '-o', 'infojson:' + outlet_object.info,
                        '--download-archive', f"{outlet_object.path}/downloaded.txt",  # File name where the download
                        '--embed-subs',                         # Embed subtitles in the output file
                        '--audio-format', 'm4a',                   # Output audio in m4a format
                        '--recode-video',  'mp4',                   # Recode video to mp4
                        '--use-postprocessor', 'FFmpegCopyStream', # Force transcode
                        '--ppa', f"CopyStream: {codec}",        # Recode video to provided codec
                        '--embed-metadata',                    # Embed the subtitles, chapters, and other metadata when possible
                        '--embed-thumbnail',                    # Embed the thumbnail
                        '--write-thumbnail',                    # Write thumbnail image to disk
                        # '--convert-thumbnails', 'png',             # Convert thumbnails to png format
                        '--write-info-json',                      # Write video metadata to a .json file
                        '--sponsorblock-mark', 'all',               # Sponsorblock marker for ads to be cut
            ]

            command = ['yt-dlp'] + ydl_args + [url]

            try:
                subprocess.run(command, check=True)
            except subprocess.CalledProcessError as error:
                print(f'Download failed with error: error')


        except Exception as e:
            print(e)


"""
@signal()
def signal_handler(signal, task, exc=None):
    output = str(signal) + "\n" + str(task) + str(task.id) + str(task.name) + "\n"
    with open('./log.txt', 'a') as file:
        file.write(output)
"""

# Create your views here.

class Initialize(APIView):

    def post(self, request, format=None, *args, **kwargs):

        #initialize()

        return Response("Initialization function called", status=status.HTTP_200_OK)

class Ping(APIView):

    def get(self, request, format=None, *args, **kwargs):
        return Response("Pong", status=status.HTTP_200_OK)


class BasicDownload(APIView):  # https://youtu.be/C0DPdy98e4c

    def post(self, request, format=None, *args, **kwargs):

        print(request.body)

        payload = json.loads(request.body)

        with YoutubeDL() as ydl:
            ydl.download([payload['url']])

        return Response("Downloading", status=status.HTTP_200_OK)

class FlowDownload(APIView):  # https://youtu.be/C0DPdy98e4c

    def post(self, request, format=None, *args, **kwargs):

        try:
            payload = json.loads(request.body)
            flow_id = payload['flow_id']

            download_thread = threading.Thread(target=yt_dl_flow, args=(flow_id,))
            download_thread.start()

            return HttpResponse('Downloading', status=200)

        except KeyError as e:
            return HttpResponse('Invalid request data', status=400)

@method_decorator(csrf_exempt, name='dispatch')
class Outlet(APIView):

    def get(self, request, format=None, *args, **kwargs):

        if len(request.body) != 0:
            parsed_json = json.loads(request.body)

            try:
                outlet_id = parsed_json['outlet_id']
            except KeyError as e:
                return HttpResponse('Invalid request data', status=400)

            # So we return the information about a particular Outlet

            outlet_object = get_object_or_404(OutletModel, id=outlet_id)

            outlet_json = serializers.serialize('json', [outlet_object])

            return JsonResponse(data=outlet_json, status=200)
        else:
            # JSON is empty, so we return the list of all Outlet objects

            outlet_objects = list(OutletModel.objects.all().values())


            if len(outlet_objects) > 0:
                outlet_jsons = json.dumps(outlet_objects)

            else:
                outlet_jsons = {}


            return HttpResponse(content= outlet_jsons, status=200)

    def post(self, request, format=None, *args, **kwargs):

        try:
            payload = json.loads(request.body)
            name = payload['name']
            path = payload['path']
            video = payload['video']
            thumbnail = payload['thumbnail']
            info = payload['info']

            # Create the Flow in the databse
            new_outlet = OutletModel(name=name, path=path, video=video, thumbnail=thumbnail, info=info)
            new_outlet.save()

            return HttpResponse(status=201)

        except KeyError as e:
            return HttpResponse('Invalid request data or database faliure', status=400)

    def delete(self, request, format=None, *args, **kwargs):

        try:
            payload = json.loads(request.body)
            outlet_id = payload['outlet_id']

            # Find requested object
            outlet_object = get_object_or_404(OutletModel, id=outlet_id)
            outlet_object.delete()

            return HttpResponse('Deleted', status=200)

        except KeyError as e:
            return HttpResponse('Invalid request data', status=400)

@method_decorator(csrf_exempt, name='dispatch')
class Flow(APIView):


    def post(self, request, format=None, *args, **kwargs):


        try:
            payload = json.loads(request.body)
            name = payload['name']
            url = payload['url']
            my_type = payload['type']
            quality = payload['quality']
            codec = payload['codec']
            outlet = int(payload['outlet'])
            schedule = str(payload['schedule'])

            flow_id = str(uuid.uuid4())


            outlet_object = OutletModel.objects.get(id = outlet)

            # Create the Flow in the databse
            new_flow = FlowModel(flow_id=flow_id, name=name, url=url, type=my_type, quality=quality, outlet=outlet_object, codec=codec, schedule=schedule)
            new_flow.save()

            #schedule_flow(flow_id=flow_id, outlet_id=str(outlet), schedule_json=schedule)

            #requests.post('http://127.0.0.1:8000/api/flow_download', json={'flow_id': flow_id})

            return JsonResponse(data={'flow_id': flow_id}, status=201)

        except KeyError as e:
            return HttpResponse('Invalid request data or database faliure', status=400)


    def delete(self, request, format=None, *args, **kwargs):

        try:
            payload = json.loads(request.body)
            flow_id = payload['flow_id']

            # Find requested object
            flow_object = get_object_or_404(FlowModel, id=flow_id)

            # Make the timer delete API call
            #response = requests.delete(
                #'http://127.0.0.1:8000/api/timer', json={'timer_id': flow_object.timer.timer_id})

            # Check the response status code and raise ValidationError if not successful
            #if not response.status_code // 100 == 2:  # check for 2XX status codes
                #return HttpResponse('Failed to delete Timer', status=500)

            flow_object.delete()

            return HttpResponse('Deleted Flow', status=200)

        except KeyError as e:
            return HttpResponse('Invalid request data', status=400)

    def get(self, request, format=None, *args, **kwargs):

        if len(request.body) != 0:
            parsed_json = json.loads(request.body)

            try:
                flow_id = parsed_json['flow_id']
            except KeyError as e:
                return HttpResponse('Invalid request data', status=400)

            # So we return the information about a particular Flow

            flow_object = get_object_or_404(FlowModel, id=flow_id)

            flow_json = serializers.serialize('json', [flow_object])

            return JsonResponse(data=flow_json, status=200)
        else:
            # JSON is empty, so we return the list of all Flow objects

            flow_objects = list(FlowModel.objects.all().values())

            if len(flow_objects) > 0:
                flow_jsons = json.dumps(flow_objects)

            else:
                flow_jsons = {}

            return HttpResponse(content=flow_jsons, status=200)

class TemplateView(APIView):

    def get(self, request, *args, **kwargs):

        template_type = kwargs.get('template_type')

        templates = TemplateModel.objects.filter(type=template_type)

        data = [{'name': template.name, 'content': template.content} for template in templates]

        return JsonResponse(data, safe=False)

class DropDownload(APIView):

    def post(self, request, format=None, *args, **kwargs):


        try:
            payload = json.loads(request.body)
            url = payload['url']
            quality = payload['quality']
            codec = payload['codec']
            outlet = int(payload['outlet'])

            flow_id = str(uuid.uuid4())


            outlet_object = OutletModel.objects.get(id = outlet)

            settings_dict = {
                "url": url,
                "quality": quality,
                "codec": codec,
                "outlet_id": outlet
            }

            settings_json = json.dumps(settings_dict)


            yt_dl_drop(settings_json=settings_json, outlet=outlet)


            return JsonResponse(data={'flow_id': flow_id}, status=201)

        except KeyError as e:
            return HttpResponse("Invalid request data or database faliure", status=400)
