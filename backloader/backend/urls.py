from django.urls import path
from . import views

urlpatterns = [
    path('ping', views.Ping.as_view()),
    path('initialize', views.Initialize.as_view()),
    path('download', views.BasicDownload.as_view()),
    path('flow', views.Flow.as_view()),
    path('flow-download', views.FlowDownload.as_view()),
    path('outlet', views.Outlet.as_view()),
    path('templates/<str:template_type>', views.TemplateView.as_view()),
    path('drop-download', views.DropDownload.as_view()),
]

# views.initialize()