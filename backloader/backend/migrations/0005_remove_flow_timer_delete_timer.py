# Generated by Django 4.2.6 on 2023-11-06 12:00

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0004_flow_codec'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='flow',
            name='timer',
        ),
        migrations.DeleteModel(
            name='Timer',
        ),
    ]
