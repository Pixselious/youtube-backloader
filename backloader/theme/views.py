import httpx
import json
import requests
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from django import forms
from django.urls import reverse


def dashboardView(request, **kwargs):

    # response = requests.get('http://127.0.0.1:8000/api/huey/scheduled')
    # response = response.text
    response = ""

    return render(request, 'home.html', {'response': response})

@csrf_exempt
def flowsView(request, **kwargs):
    response = requests.get('http://127.0.0.1:8000/api/flow')
    if response.status_code != 200 or response.text == '':
        processed = {}
    else:
        processed = response.json()

    return render(request, 'flows/flows.html', {'flows': processed})


class CreateFlowForm(forms.Form):

    QUALITY_CHOICES = [
        ('a', 'Audio'),
        ('720', '720p'),
        ('1080', '1080p'),
        ('1440', '1440p'),
        ('2160', '4k'),
        ('max', 'Best')
    ]

    TYPE_CHOICES = [
        ('p', 'Playlist'),
        ('c', 'Channel (not implemented)')
    ]

    name = forms.CharField()
    url = forms.URLField()
    type = forms.ChoiceField(choices=TYPE_CHOICES)
    quality = forms.ChoiceField(choices=QUALITY_CHOICES)
    codec = forms.CharField()
    outlet = forms.ChoiceField(choices=[])
    schedule = forms.CharField()


    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # make API request to get outlets data
        response = requests.get('http://127.0.0.1:8000/api/outlet')
        try:
            outlets = [(outlet['id'], outlet['name'])
                   for outlet in response.json()]
        except:
            outlets = []

        # populate choices for outlet field
        self.fields['outlet'].choices = outlets

    def is_valid(self):
        return True


    def submit(self):
        api_url = 'http://127.0.0.1:8000/api/flow'


        name = self.data['name']
        url = self.data['url']
        type = self.data['type']
        quality = self.data['quality']
        codec = self.data['codec']
        outlet = self.data['outlet']
        schedule = self.data['schedule']

        # prepare post data and make API call
        data = {
            'name': name,
            'url': url,
            'type': type,
            'quality': quality,
            'codec': codec,
            'outlet': outlet,
            'schedule': schedule,
        }

        response = requests.post(api_url, json=data)
        response.raise_for_status()

@csrf_exempt
def createFlowView(request, **kwargs):
    form = CreateFlowForm(request.POST)

    if request.method == 'POST' and form.is_valid():
        form.submit()
        return redirect(reverse('flows'))

    # make API request to get outlets data
    response = requests.get('http://127.0.0.1:8000/api/templates/schedule')
    try:
        schedule_templates = response.json()
    except:
        schedule_templates = []

     # make API request to get outlets data
    response = requests.get('http://127.0.0.1:8000/api/templates/codec')
    try:
        codec_templates = response.json()
    except:
        codec_templates = []


    # Pass the schedule templates and the form to the template
    context = {
        'form': form,
        'schedule_templates': schedule_templates,
        'codec_templates': codec_templates
    }

    return render(request, 'flows/create.html', context)

@csrf_exempt
def outletsView(request, **kwargs):

    response = requests.get('http://127.0.0.1:8000/api/outlet')
    if response.status_code != 200 or response.text == '':
        processed = {}
    else:
        processed = response.json()

    return render(request, 'outlets/outlets.html', {'outlets': processed})


class CreateOutletForm(forms.Form):
    name = forms.CharField(max_length=255)
    path = forms.CharField(max_length=255)
    video = forms.CharField(max_length=255)
    thumbnail = forms.CharField(max_length=255)
    info = forms.CharField(max_length=255)

    def is_valid(self):

        return True


    def submit(self):
        api_url = 'http://127.0.0.1:8000/api/outlet'

        # get form data
        name = self.data['name']
        path = self.data['path']
        video = self.data['video']
        thumbnail = self.data['thumbnail']
        info = self.data['info']




        # prepare post data and make API call
        data = {
            'name': name,
            'path': path,
            'video': video,
            'thumbnail': thumbnail,
            'info': info,
        }


        response = requests.post(api_url, json=data)
        response.raise_for_status()

@csrf_exempt
def createOutletView(request):

    form = CreateOutletForm(request.POST)

    if request.method == 'POST' and form.is_valid():


        form.submit()
        return redirect(reverse('outlets'))
    else:
        form = CreateOutletForm()

        # make API request to get outlets data
        response = requests.get('http://127.0.0.1:8000/api/templates/outlet')
        try:
            outlet_templates = response.json()
        except:
            outlet_templates = []

        context = {
            'form': form,
            'outlet_templates': outlet_templates
        }

    return render(request, 'outlets/create.html', context)

@csrf_exempt
def settingsView(request, **kwargs):

    response = requests.get('http://127.0.0.1:8000/api/ping')
    response_text = response.text

    return render(request, 'settings.html', {'response': response_text})


class dropForm(forms.Form):

    QUALITY_CHOICES = [
        ('a', 'Audio'),
        ('720', '720p'),
        ('1080', '1080p'),
        ('1440', '1440p'),
        ('2160', '4k'),
        ('max', 'Best')
    ]

    url = forms.CharField(max_length=255)
    quality = forms.ChoiceField(choices=QUALITY_CHOICES)
    codec = forms.CharField()
    outlet = forms.ChoiceField(choices=[])

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # make API request to get outlets data
        response = requests.get('http://127.0.0.1:8000/api/outlet')
        try:
            outlets = [(outlet['id'], outlet['name'])
                   for outlet in response.json()]
        except:
            outlets = []

        # populate choices for outlet field
        self.fields['outlet'].choices = outlets

    def is_valid(self):

        return True


    def submit(self):
        api_url = 'http://127.0.0.1:8000/api/drop-download'

        # get form data
        url = self.data['url']
        quality = self.data['quality']
        codec = self.data['codec']
        outlet = self.data['outlet']

        # prepare post data and make API call
        data = {
            'url': url,
            'outlet': outlet,
            "quality": quality,
            "codec": codec,
        }


        response = requests.post(api_url, json=data)
        response.raise_for_status()

@csrf_exempt
def createDropView(request):



    form = dropForm(request.POST)

    if request.method == 'POST' and form.is_valid():


        form.submit()
        return redirect(reverse('drops-done'))
    else:
        form = dropForm()

         # make API request to get outlets data
        response = requests.get('http://127.0.0.1:8000/api/templates/codec')
        try:
            codec_templates = response.json()
        except:
            codec_templates = []

        # Pass the schedule templates and the form to the template
        context = {
            'form': form,
            'codec_templates': codec_templates
        }

    return render(request, 'drops/create.html', context)

@csrf_exempt
def dropsView(request, **kwargs):

    return render(request, 'drops/drops.html')

@csrf_exempt
def dropsDoneView(request, **kwargs):

    return render(request, 'drops/done.html')
